package bzh.alybubu.jour20;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static bzh.alybubu.jour20.Part1.Impulsion.BASSE;
import static bzh.alybubu.jour20.Part1.Impulsion.HAUTE;

@Slf4j
public class Part1 {

    private final String entree = """
            %gv -> lq, pm
            %rv -> jd, nh
            %nh -> rs, jd
            &vt -> tj
            %zv -> pm, gv
            %gh -> jd, vd
            %hh -> bf, qm
            %kx -> nf
            %st -> pm, zc
            %bh -> qm, pv
            &sk -> tj
            %hl -> nf, pn
            %mt -> st, pm
            &jd -> ts, gh, vd, dc, xc
            %zm -> hm
            %pv -> vv
            %zf -> nf, cz
            &xc -> tj
            %bf -> qm
            %ts -> sg
            %ht -> ch, nf
            %pb -> rv, jd
            %nx -> fc
            %mb -> mt
            %mh -> jd, pb
            %lc -> bh
            %xg -> mb, pm
            %vd -> dc
            broadcaster -> gh, dl, xg, fb
            %sg -> mh, jd
            %qq -> ts, jd
            %dl -> nf, sv
            %vv -> sm, qm
            %zc -> tb
            %sr -> zv, pm
            %dc -> gb
            %cz -> nf, zm
            %rs -> jd
            %hm -> nf, hl
            %gd -> sr
            &qm -> lc, pv, nx, fb, kk
            &tj -> rx
            %gb -> qq, jd
            %xf -> zf
            %tb -> lg
            %sm -> qm, hh
            %fb -> dr, qm
            %lq -> pm
            &nf -> zm, dl, ch, xf, vt
            &pm -> sk, zc, tb, gd, mb, xg
            %pn -> nf, kx
            %fc -> xb, qm
            %ch -> xf
            &kk -> tj
            %lg -> pm, gd
            %sv -> nf, ht
            %xb -> qm, lc
            %dr -> nx, qm
            """;

    // Provenance -> Impulsion -> Destination
    private final LinkedList<Triple<String, Impulsion, String>> impulsions = new LinkedList<>();
    private int compteurBasse = 0;
    private int compteurHaute = 0;

    @Test
    void jour20() {
        var modules = Arrays.stream(entree.split("\n"))
                .map(s -> s.replace(" ", ""))
                .map(this::construireModule)
                .collect(Collectors.toList());

        // Test si des modules destinations n'existent pas
        var modulesInexistants = modules.stream()
                .map(IModule::getDestinations)
                .flatMap(Collection::stream)
                .distinct()
                .filter(m -> modules.stream().noneMatch(module -> module.getNom().equals(m)))
                .map(BasicModule::new)
                .toList();

        // Ajout des inexistants
        modules.addAll(modulesInexistants);

        // On va remplir les entrées des modules conjonction
        modules.stream()
                .filter(m -> m instanceof ConjonctionModule)
                .map(m -> (ConjonctionModule) m)
                .forEach(m -> remplirEntreesConjonctions(m, modules));

        var compteur = 0;

        // Appuie sur bouton
        //log.debug("------------------ {} ----------------------------------", compteur + 1);
        impulsions.add(Triple.of("Bouton", BASSE, "broadcaster"));
        compteur++;

        while (!impulsions.isEmpty()) {
            // log.debug("Impulsions : {}", impulsions);
            // Traitement de l'impulsion
            traiterImpulsion(impulsions.peek(), modules);
            // Retrait car traitée
            impulsions.remove();

            // Si toutes les impulsions ont été traités, appuie de nouveau sur le bouton
            if (impulsions.isEmpty() && compteur < 1000) {
                //log.debug("------------------ {} ----------------------------------", compteur + 1);
                // Appuie sur bouton
                impulsions.add(Triple.of("Bouton", BASSE, "broadcaster"));
                compteur++;
            }
        }

        log.debug("Sortie : {}", compteurHaute * compteurBasse);
    }


    // Provenance -> Impulsion -> Destination
    void traiterImpulsion(Triple<String, Impulsion, String> impulsion, List<IModule> modules) {
        var moduleDestinationNom = impulsion.getRight();
        var moduleProvenanceNom = impulsion.getLeft();
        var impulsionValeur = impulsion.getMiddle();

        if (HAUTE.equals(impulsionValeur)) {
            compteurHaute++;
        } else {
            compteurBasse++;
        }

        var moduleDestination = modules.stream()
                .filter(m -> m.getNom().equals(moduleDestinationNom))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Impossible de trouver le module nommé " + moduleDestinationNom));


        var retour = moduleDestination.traiterImpulsion(Pair.of(moduleProvenanceNom, impulsionValeur))
                .stream()
                .map(p -> Triple.of(moduleDestination.getNom(), p.getLeft(), p.getRight()))
                .toList();

        impulsions.addAll(retour);
    }

    private void remplirEntreesConjonctions(ConjonctionModule moduleConjonction, List<IModule> modules) {
        var entrees = modules.stream()
                .filter(m -> m.getDestinations().contains(moduleConjonction.getNom()))
                .map(IModule::getNom)
                .collect(Collectors.toMap(n -> n, n -> BASSE));

        moduleConjonction.getEntrees().putAll(entrees);
    }

    IModule construireModule(String chaine) {
        var elem = chaine.split("->");
        var destinations = Arrays.asList(elem[1].split(","));

        // Broadcast
        if ("broadcaster".equals(elem[0])) {
            return new BroadcastModule(elem[0], destinations);
        }
        // Interrupteur
        else if (elem[0].startsWith("%")) {
            return new InterrupteurModule(elem[0].replace("%", ""), destinations);
        }
        // Conjonction
        else if (elem[0].startsWith("&")) {
            return new ConjonctionModule(elem[0].replace("&", ""), destinations);
        } else {
            throw new RuntimeException("Impossible de construire le module pour la chaine " + chaine);
        }
    }

    enum Impulsion {
        HAUTE,
        BASSE
    }

    interface IModule {
        String getNom();

        List<String> getDestinations();

        // Sortie : Impulsion - Destination
        // Entree : Provenance - Impulsion
        List<Pair<Impulsion, String>> traiterImpulsion(Pair<String, Impulsion> impulsion);
    }

    record BroadcastModule(String nom, List<String> destinations) implements IModule {
        @Override
        public String getNom() {
            return nom;
        }

        @Override
        public List<String> getDestinations() {
            return destinations;
        }

        // Sortie : Impulsion - Destination
        // Entree : Provenance - Impulsion
        @Override
        public List<Pair<Impulsion, String>> traiterImpulsion(Pair<String, Impulsion> impulsion) {
            // On ne fait que transmettre le signal aux destinations
            return destinations
                    .stream()
                    .map(d -> Pair.of(impulsion.getRight(), d))
                    .toList();
        }
    }

    record BasicModule(String nom) implements IModule {
        @Override
        public String getNom() {
            return nom;
        }

        @Override
        public List<String> getDestinations() {
            return List.of();
        }

        // Sortie : Impulsion - Destination
        // Entree : Provenance - Impulsion
        @Override
        public List<Pair<Impulsion, String>> traiterImpulsion(Pair<String, Impulsion> impulsion) {
            return List.of();
        }
    }

    @Getter
    @RequiredArgsConstructor
    static class InterrupteurModule implements IModule {
        private final String nom;
        private final List<String> destinations;
        // true : ON
        // false : OFF
        // éteint par défaut
        private boolean etat = false;

        @Override
        public String getNom() {
            return nom;
        }

        @Override
        public List<String> getDestinations() {
            return destinations;
        }

        // Sortie : Impulsion - Destination
        // Entrée : Provenance - Impulsion
        @Override
        public List<Pair<Impulsion, String>> traiterImpulsion(Pair<String, Impulsion> impulsion) {
            var valeurImpulsion = impulsion.getRight();

            // Impulsion haute : ignorée, rien ne se passe
            if (HAUTE.equals(valeurImpulsion)) {
                return List.of();
            }
            // Impulsion basse
            else {
                // Switche de ON à OFF (et vice versa)
                etat = !etat;
                Impulsion imp;

                // ON : HAUTE
                if (etat) {
                    imp = HAUTE;
                }
                // OFF : BASSE
                else {
                    imp = BASSE;
                }

                // Envoi du signal à toutes les destinations
                return destinations
                        .stream()
                        .map(d -> Pair.of(imp, d))
                        .toList();
            }
        }
    }

    @RequiredArgsConstructor
    static class ConjonctionModule implements IModule {

        private final String nom;
        private final List<String> destinations;

        @Getter
        private final Map<String, Impulsion> entrees = new HashMap<>();

        @Override
        public String getNom() {
            return nom;
        }

        @Override
        public List<String> getDestinations() {
            return destinations;
        }

        // Sortie : Impulsion - Destination
        // Entrée : Provenance - Impulsion
        @Override
        public List<Pair<Impulsion, String>> traiterImpulsion(Pair<String, Impulsion> impulsion) {
            // Mise à jour de la mémoire
            entrees.put(impulsion.getLeft(), impulsion.getRight());

            Impulsion imp;

            // Si il a en mémoire pour toutes ses entrées HAUTE -> envoie BASSE
            if (entrees.values()
                    .stream()
                    .allMatch(HAUTE::equals)) {
                imp = BASSE;
            } else {
                imp = HAUTE;
            }

            // Envoi du signal à toutes les destinations
            return destinations
                    .stream()
                    .map(d -> Pair.of(imp, d))
                    .toList();
        }
    }
}
