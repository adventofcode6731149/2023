package bzh.alybubu.jour2;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class Jour2 {

    // Les valeurs brutes en entrée
    private String entree() {
        return """
                 Game 1: 1 green, 6 red, 4 blue; 2 blue, 6 green, 7 red; 3 red, 4 blue, 6 green; 3 green; 3 blue, 2 green, 1 red
                ...
                 Game 100: 3 blue, 6 red, 9 green; 4 red, 3 green; 4 green, 16 red, 1 blue; 14 blue, 1 green
                 """;
    }

    @Test
    void test() {

        var sortie = Arrays.stream(entree().split("\n"))
                .map(s -> s.replace("Game", ""))
                .map(s -> s.replace(" ", ""))
                .map(s -> s.split(":"))
                .collect(Collectors.toMap(e -> Integer.valueOf(e[0]), e -> transformerValeurs(e[1])))
                .values()
                .stream()
                .mapToInt(this::transformerValeur)
                .sum();

        log.debug("Sortie : {}", sortie);
    }


    private int transformerValeur(List<List<Map<String, Integer>>> valeur) {
        var red = maxCouleur(valeur, "red");
        var green = maxCouleur(valeur, "green");
        var blue = maxCouleur(valeur, "blue");

        return red * green * blue;
    }

    private int maxCouleur(List<List<Map<String, Integer>>> valeur, String couleur) {
        return valeur.stream()
                .flatMap(Collection::stream)
                .filter(e -> e.containsKey(couleur))
                .map(Map::values)
                .flatMap(Collection::stream)
                .max(Integer::compareTo)
                .orElse(0);
    }

    private List<List<Map<String, Integer>>> transformerValeurs(String valeur) {
        return Arrays.stream(valeur.split(";"))
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .map(this::transformerEnCleValeur)
                .toList();
    }

    private List<Map<String, Integer>> transformerEnCleValeur(List<String> valeurs) {
        var pattern = Pattern.compile("([0-9]+)(\\w+)");

        return valeurs
                .stream()
                .map(pattern::matcher)
                .map(m -> {
                    if (m.find()) {
                        return Pair.of(Integer.valueOf(m.group(1)), m.group(2));
                    } else {
                        throw new RuntimeException("Erreur"); // n'arrivera pas
                    }
                })
                .map(p -> Map.of(p.getRight(), p.getLeft()))
                .toList();
    }

}
