package bzh.alybubu.jour23;

import bzh.alybubu.commun.Position;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
class Part1 {

    private final String entree2 = """
            #.#####################
            #.......#########...###
            #######.#########.#.###
            ###.....#.>.>.###.#.###
            ###v#####.#v#.###.#.###
            ###.>...#.#.#.....#...#
            ###v###.#.#.#########.#
            ###...#.#.#.......#...#
            #####.#.#.#######.#.###
            #.....#.#.#.......#...#
            #.#####.#.#.#########v#
            #.#...#...#...###...>.#
            #.#.#v#######v###.###v#
            #...#.>.#...>.>.#.###.#
            #####v#.#.###v#.#.###.#
            #.....#...#...#.#.#...#
            #.#########.###.#.#.###
            #...###...#...#...#.###
            ###.###.#.###v#####v###
            #...#...#.#.>.>.#.>.###
            #.###.###.#.###.#.#v###
            #.....###...###...#...#
            #####################.#
            """;

    private final String entree = """
            #.###########################################################################################################################################
            #.......#...#...###...#.....#...#.....#...#####...#...............#...#...#...#.......#.......#...###...#####.......###.........###.......###
            #######.#.#.#.#.###.#.#.###.#.#.#.###.#.#.#####.#.#.#############.#.#.#.#.#.#.#.#####.#.#####.#.#.###.#.#####.#####.###.#######.###.#####.###
            #.......#.#.#.#.#...#.#...#.#.#.#...#.#.#.....#.#.#...........#...#.#.#.#.#.#.#.#.....#.....#...#.....#...#...#...#.....#.......#...#...#...#
            #.#######.#.#.#.#.###.###.#.#.#.###.#.#.#####.#.#.###########.#.###.#.#.#.#.#.#.#.#########.#############.#.###.#.#######.#######.###.#.###.#
            #...#...#.#.#.#.#...#.....#...#...#.#...#.....#.#.#...###...#.#...#.#...#...#.#.#...#.>.>.#...#.........#.#...#.#.........###.....#...#.....#
            ###.#.#.#.#.#.#.###.#############.#.#####.#####.#.#.#.###.#.#.###.#.#########.#.###.#.#v#.###.#.#######.#.###.#.#############.#####.#########
            ###...#.#.#.#.#.###.......#...#...#.....#.....#.#.#.#.#...#...#...#...###.....#...#.#.#.#...#.#.#...###...#...#.......###...#...#...#.......#
            #######.#.#.#.#.#########.#.#.#.#######.#####.#.#.#.#.#.#######.#####.###.#######.#.#.#.###.#.#.#.#.#######.#########.###.#.###.#.###.#####.#
            #.......#.#.#.#.#...###...#.#...#...#...#.....#.#.#.#.#.......#.....#...#.....#...#...#...#.#.#.#.#...#.>.>.#...#.....#...#...#.#...#.#.....#
            #.#######.#.#.#.#.#.###.###.#####.#.#.###.#####.#.#.#.#######.#####.###.#####.#.#########.#.#.#.#.###.#.#v###.#.#.#####.#####.#.###.#.#.#####
            #.#.....#.#.#.#.#.#...#...#.>.>.#.#.#.#...#...#.#.#.#...###...#.....#...#.....#.#...###...#.#.#.#.#...#.#.#...#...#...#.#.....#.#...#.#...###
            #.#.###.#.#.#.#.#.###.###.###v#.#.#.#.#.###.#.#.#.#.###.###.###.#####.###.#####.#.#.###.###.#.#.#.#.###.#.#.#######.#.#.#.#####.#.###.###.###
            #.#.#...#.#.#.#.#...#.#...#...#.#.#.#.#.###.#.#.#.#.#...#...#...#...#.###...#...#.#.#...###...#...#.....#.#.#.....#.#.#.#.###...#...#...#.###
            #.#.#.###.#.#.#v###.#.#.###.###.#.#.#.#.###.#.#.#.#.#.###.###.###.#.#.#####.#.###.#.#.###################.#.#.###.#.#.#.#.###.#####.###.#.###
            #.#.#.#...#...#.>.#.#.#.###...#.#.#.#.#.#...#...#...#.>.>.###.#...#.#...#...#.#...#...#...#.......#.......#.#.#...#.#...#.###.#.....#...#...#
            #.#.#.#.#######v#.#.#.#.#####.#.#.#.#.#.#.#############v#####.#.###.###.#.###.#.#######.#.#.#####.#.#######.#.#.###.#####.###.#.#####.#####.#
            #.#.#...#.......#...#.#.#####.#.#.#.#.#.#.......###...#.....#.#...#.#...#...#.#.........#.#.#...#.#.......#.#.#...#.....#.###.#.#...#...#...#
            #.#.#####.###########.#.#####.#.#.#.#.#.#######.###.#.#####.#.###.#.#.#####.#.###########.#.#.#.#.#######.#.#.###.#####.#.###.#.#.#.###.#.###
            #...#...#...........#.#.#.....#...#...#.#.....#.#...#.......#...#.#.#...###...#.........#.#...#.#.#...#...#.#...#.#...#.#.#...#...#...#.#.###
            #####.#.###########.#.#.#.#############.#.###.#.#.#############.#.#.###.#######.#######.#.#####.#.#.#.#.###.###.#.#.#.#.#.#.#########.#.#.###
            #.....#.............#...#.............#.#...#...#...#.......###...#.....#.....#.......#.#.#.....#...#...###.#...#.#.#...#...#...#...#...#...#
            #.###################################.#.###.#######.#.#####.#############.###.#######.#.#.#.###############.#.###.#.#########.#.#.#.#######.#
            #.#...............#...#...............#.....###...#...#.....#...........#...#.....#...#...#...............#...###...###...#...#.#.#.......#.#
            #.#.#############.#.#.#.#######################.#.#####.#####.#########.###.#####.#.#####################.#############.#.#.###.#.#######.#.#
            #.#.#...#...#.....#.#.#.......###...#.....#...#.#.#...#.....#...#.......#...#...#...#...#...#.............#...#.....#...#.#...#.#.......#.#.#
            #.#.#.#.#.#.#.#####.#.#######.###.#.#.###.#.#.#.#.#.#.#####.###.#.#######.###.#.#####.#.#.#.#.#############.#.#.###.#.###.###.#.#######.#.#.#
            #.#.#.#...#...#...#.#.#...###...#.#.#.#...#.#.#.#.#.#.###...###.#...#...#.....#.......#...#.#.............#.#.#.#...#.#...#...#.........#.#.#
            #.#.#.#########.#.#.#.#.#.#####.#.#.#.#.###.#.#.#.#.#.###v#####.###.#.#.###################.#############.#.#.#.#.###.#.###v#############.#.#
            #.#.#.#...#...#.#.#.#.#.#.#...#...#...#...#.#.#.#...#.#.>.>.###.#...#.#.#...###...#.........#...#.........#.#...#...#.#.#.>.#...#...#####...#
            #.#.#.#.#v#.#.#.#.#.#.#.#.#.#.###########v#.#.#.#####.#.#v#.###.#.###.#.#.#.###.#.#.#########.#.#.#########.#######.#.#.#.#v#.#.#.#.#########
            #.#.#.#.#.>.#.#.#.#.#.#.#.#.#.#...#.....>.>.#.#.#...#...#.#.#...#...#.#.#.#...#.#.#...#.....#.#.#.........#.......#.#.#.#.#.#.#.#.#.........#
            #.#.#.#.#v###.#.#.#.#.#.#.#.#.#.#.#.#####v###.#.#.#.#####.#.#.#####.#.#.#.###.#.#.###.#.###.#.#.#########.#######.#.#.#.#.#.#.#.#.#########.#
            #...#...#...#.#.#.#.#.#.#.#.#.#.#.#.#.....#...#.#.#...#...#...###...#.#.#.#...#.#.#...#.#...#.#.#...###...#...#...#.#.#.#.#.#.#.#.#...#.....#
            ###########.#.#.#.#.#.#.#.#.#.#.#.#.#.#####.###.#.###.#.#########.###.#.#.#.###.#.#v###.#.###.#.#.#.###v###.#.#.###.#.#.#.#.#.#.#.#.#.#.#####
            #...........#...#...#.#.#.#.#.#.#...#.....#.....#...#.#.....#.....#...#.#.#...#.#.>.>...#.#...#.#.#...>.>.#.#.#...#...#.#.#.#.#.#.#.#...#####
            #.###################.#.#.#.#.#.#########.#########.#.#####.#.#####.###.#.###.#.###v#####.#.###.#.#####v#.#.#.###.#####.#.#.#.#.#.#.#########
            #.#.............#...#...#.#.#...#...#...#.......#...#.......#...#...#...#...#...###...###.#...#.#.#.....#...#...#.....#...#...#.#.#...#...###
            #.#.###########.#.#.#####.#.#####.#.#.#.#######.#.#############.#.###.#####.#########.###.###.#.#.#.###########.#####.#########.#.###.#.#.###
            #.#.#...........#.#.#...#.#.#.....#...#...#...#.#...........###.#...#...#...#.......#...#.#...#.#.#...........#.#.....###.......#...#...#...#
            #.#.#.###########.#.#.#.#.#.#.###########.#.#.#.###########.###.###.###.#.###.#####.###.#.#.###.#.###########.#.#.#######.#########.#######.#
            #...#.....#...#...#...#.#...#.....#...###.#.#...#...###...#.#...#...#...#.###.#...#.....#...###.#.#...........#.#.#.....#.......#...#.......#
            #########.#.#.#.#######.#########.#.#.###.#.#####.#.###.#.#.#.###.###.###.###.#.#.#############.#.#.###########.#.#.###.#######.#.###.#######
            #...#...#...#...#.....#.#...#...#...#...#.#...#...#.....#...#...#...#...#.#...#.#.#...#.......#.#.#.#.......#...#...#...#.......#.#...#######
            #.#.#.#.#########.###.#.#.#.#.#.#######.#.###.#.###############.###.###.#.#.###.#.#.#.#.#####.#.#.#.#.#####.#.#######.###.#######.#.#########
            #.#...#...........###...#.#.#.#.###.....#...#.#.............###.....###...#.....#...#.#.#.....#...#...#.....#.....#...###...#.....#.........#
            #.#######################.#.#.#.###.#######.#.#############.#########################.#.#.#############.#########.#.#######.#.#############.#
            #.................#.......#...#...#...#...#...#.............#...#.............#...#...#.#.###...#.......###...###...#...###...###...#.......#
            #################.#.#############.###.#.#.#####.#############.#.#.###########.#.#.#.###.#.###.#.#.#########.#.#######.#.#########.#.#.#######
            #.....#...........#.............#.....#.#...###...............#.#.........#...#.#.#.....#.....#.#.#.........#.....###.#.#...#...#.#.#.......#
            #.###.#.#######################.#######.###.###################.#########.#.###.#.#############.#.#.#############.###.#.#.#.#.#.#.#.#######.#
            #...#.#.......#...###...#...###.....###.#...#...#...............###...#...#...#.#.#.............#...###.....#...#...#.#...#.#.#...#.........#
            ###.#.#######v#.#.###.#.#.#.#######.###.#.###.#.#.#################.#.#.#####.#.#.#.###################.###.#.#.###.#.#####.#.###############
            #...#...#...#.>.#...#.#.#.#.###...#.#...#.....#.#.#.....#...#...#...#.#.....#.#.#.#.............#.......###...#.#...#.....#.#...............#
            #.#####.#.#.#v#####.#.#.#.#.###.#.#v#.#########.#.#.###.#.#.#.#.#.###.#####.#.#.#.#############.#.#############.#.#######.#.###############.#
            #.#...#...#...###...#.#.#.#.#...#.>.>.#.....#...#...#...#.#.#.#.#.#...#...#.#.#.#.###...........#.............#...#...#...#...#.........#...#
            #.#.#.###########.###.#.#.#.#.#####v###.###.#.#######.###.#.#.#.#.#.###.#.#.#.#.#.###v#######################.#####.#.#.#####.#.#######.#.###
            #.#.#...#...#...#...#.#.#.#.#.#.....###...#...###...#...#.#.#.#...#...#.#.#.#...#...>.>.###...###.........#...###...#.#.....#.#.......#...###
            #.#.###.#.#.#.#.###.#.#.#.#.#.#.#########.#######.#.###.#.#.#.#######.#.#.#.#########v#.###.#.###.#######.#.#####.###.#####.#.#######v#######
            #.#.###...#.#.#...#...#...#...#.......###...###...#.....#.#...#...#...#.#...###...#...#...#.#...#.......#...#...#...#.#...#.#.#...#.>.#...###
            #.#.#######.#.###.###################.#####.###.#########.#####.#.#.###.#######.#.#.#####.#.###.#######.#####.#.###.#.#.#.#.#.#.#.#.#v#.#.###
            #...#.......#.###...#...#...#...#...#.....#...#.......#...#...#.#.#.....#.......#...#...#.#.#...#.....#...#...#.#...#...#...#...#...#...#...#
            #####.#######.#####.#.#.#.#.#.#.#.#.#####.###.#######v#.###.#.#.#.#######.###########.#.#.#.#.###.###.###.#.###.#.#########################.#
            #...#.........#...#...#.#.#...#...#.....#.###...#...>.>.###.#...#.......#...........#.#.#.#.#...#.#...#...#...#.#.....#.....................#
            #.#.###########.#.#####.#.#############.#.#####.#.###v#####.###########.###########.#.#.#.#.###.#.#.###v#####.#.#####.#.#####################
            #.#.#...###.....#.......#.............#...#.....#...#.#...#.#...........#.........#...#.#...#...#.#.#.>.>...#.#...#...#.....................#
            #.#.#.#.###.#########################.#####.#######.#.#.#.#.#.###########.#######.#####.#####.###.#.#.#v###.#.###.#.#######################.#
            #.#...#...#...#.......#...#...........#...#.......#.#.#.#.#.#.....#...###.......#.#...#...###.....#...#.#...#...#.#...#...#...###.....#.....#
            #.#######.###.#.#####.#.#.#.###########.#.#######.#.#.#.#.#.#####.#.#.#########.#.#.#.###.#############.#.#####.#.###.#.#.#.#.###.###.#.#####
            #...#...#...#...#.....#.#.#.........###.#...#...#.#.#.#.#...#.....#.#...#.......#...#.#...#.............#.......#.#...#.#...#...#...#.#.....#
            ###.#.#.###.#####.#####.#.#########.###.###.#.#.#.#.#.#.#####.#####.###.#.###########.#.###.#####################.#.###.#######.###.#.#####.#
            ###...#.#...#...#.....#.#.........#.#...#...#.#.#...#...#...#.......#...#...#...#...#.#...#.....#...#...###...###...###.......#.....#.#...#.#
            #######.#.###.#.#####.#.#########.#.#.###.###.#.#########.#.#########.#####.#.#.#.#.#.###.#####.#.#.#.#.###.#.###############.#######.#.#.#.#
            #.......#.....#.......#.........#...#...#.....#...#...#...#...........#####...#...#.#.#...###...#.#...#.....#.....###.........#...###.#.#...#
            #.#############################.#######.#########.#.#.#.###########################.#.#.#####.###.###############.###.#########.#.###.#.#####
            #.#.....#.................#.....#...#...#...#.....#.#.#.................#...#...#...#...#...#.#...#.............#...#.#.........#...#.#.....#
            #.#.###.#.###############.#.#####.#.#.###.#.#.#####.#.#################.#.#.#.#.#.#######.#.#.#.###.###########.###.#.#.###########.#.#####.#
            #.#.#...#.#...........#...#...###.#.#.....#.#.#...#.#.#...#...#.........#.#.#.#.#...#...#.#.#...###.......#...#...#.#.#.#...........#...#...#
            #.#.#.###.#.#########.#.#####.###.#.#######.#.#.#.#.#.#.#.#.#.#v#########.#.#.#.###.#.#.#.#.#############.#.#.###.#.#.#.#.#############.#.###
            #...#...#.#.....#...#...#...#.#...#.#...#...#.#.#.#.#.#.#...#.>.>.....#...#.#.#.#...#.#...#.#...#...#...#...#...#...#...#.............#.#...#
            #######.#.#####.#.#v#####.#.#v#.###.#.#.#.###.#.#.#.#.#.#######v#####.#.###.#.#.#.###.#####.#.#.#.#.#.#.#######.#####################.#.###.#
            #.....#...#...#...#.>...#.#.>.>.###...#.#.#...#.#.#.#.#...#.....#...#.#.#...#.#.#...#.#.....#.#.#.#.#.#.#.....#...#...#...###.....#...#.....#
            #.###.#####.#.#####v###.#.###v#########.#.#.###.#.#.#.###.#.#####.#.#.#.#.###.#.###v#.#.#####.#.#.#.#.#.#v###.###.#.#.#.#.###.###.#.#########
            #...#.#...#.#.......###...#...###...#...#.#.#...#.#.#...#.#.....#.#.#.#.#...#.#.#.>.>.#...#...#...#.#.#.>.>.#.....#.#.#.#.#...#...#.........#
            ###.#.#.#.#.###############.#####.#.#.###.#.#.###.#.###.#.#####.#.#.#.#.###.#.#.#.#v#####.#.#######.#.###v#.#######.#.#.#.#.###.###########.#
            #...#.#.#.#...............#.....#.#.#.....#.#.#...#...#.#.#...#...#.#.#.###.#.#...#.....#.#.###.....#.###.#.......#.#.#.#.#...#.........#...#
            #.###.#.#.###############.#####.#.#.#######.#.#.#####.#.#.#.#.#####.#.#.###.#.#########.#.#.###.#####.###.#######.#.#.#.#.###v#########.#.###
            #...#...#.....#...#.......#.....#.#.#...###...#...#...#.#.#.#.......#.#...#...###.......#.#...#.#...#.#...#.....#...#...#...>.#.......#.#...#
            ###.#########.#.#.#.#######.#####.#.#.#.#########.#.###.#.#.#########.###.#######.#######.###.#.#.#.#.#.###.###.#############v#.#####.#.###.#
            ###.........#.#.#...#.....#.......#...#.......###...###...#.#...#...#.#...###...#.......#.....#.#.#.#.#...#.#...###...#.......#.#.....#.....#
            ###########.#.#.#####.###.###################.#############.#.#.#.#.#.#.#####.#.#######.#######.#.#.#.###.#.#.#####.#.#.#######.#.###########
            #...........#...#...#.#...#.............#.....#.............#.#...#.#...#.....#.........#...###...#.#.###...#.......#.#.#.......#...#...#...#
            #.###############.#.#.#.###.###########.#.#####.#############.#####.#####.###############.#.#######.#.###############.#.#.#########.#.#.#.#.#
            #...........#.....#...#...#...........#...#...#.....#.........#...#.....#.#...#...........#.....###...#...#.....#.....#...###.....#...#...#.#
            ###########.#.###########.###########.#####.#.#####.#.#########.#.#####.#.#.#.#.###############.#######.#.#.###.#.###########.###.#########.#
            #...........#.#...........###...#...#.....#.#.#...#...#.........#.#...#.#.#.#...#.........#.....#.......#.#...#...#...#...#...###...#...#...#
            #.###########.#.#############.#.#.#.#####.#.#.#.#.#####.#########.#.#.#.#.#.#####.#######.#.#####.#######.###.#####.#.#.#.#.#######.#.#.#.###
            #.............#.....###.......#...#.......#.#.#.#.#...#.........#...#...#...#...#.......#.#.#...#.....#...###.#...#.#.#.#.#.....###...#.#.###
            ###################.###.###################.#.#.#.#.#.#########.#############.#.#######.#.#.#.#.#####.#.#####.#.#.#.#.#.#.#####.#######.#.###
            ###...#...#...#...#...#.......#.....#...###.#.#.#.#.#.....#...#.........###...#...#.....#...#.#...###.#.#...#.#.#.#.#.#.#.#...#...#.....#...#
            ###.#.#.#.#.#.#.#.###.#######.#.###.#.#.###.#.#.#.#.#####.#.#.#########.###.#####.#.#########.###.###.#.#.#.#v#.#.#.#.#.#.#.#.###v#.#######.#
            #...#...#...#...#.....#...###.#...#...#.....#.#.#.#.....#...#.........#.#...#...#...###.....#...#.#...#.#.#.>.>.#.#.#.#.#.#.#.#.>.#.......#.#
            #.#####################.#.###v###.###########.#.#.#####.#############.#.#.###.#.#######.###.###.#.#.###.#.###v###.#.#.#.#.#.#.#.#v#######.#.#
            #.#.....#...#.........#.#.#.>.>...#.......#...#.#.#...#...#.....#...#...#.....#.......#.#...#...#...###...###...#.#.#...#...#...#.#...###...#
            #.#.###.#.#.#.#######.#.#.#.#v#####.#####.#.###.#.#.#.###.#.###.#.#.#################.#.#.###.#################.#.#.#############.#.#.#######
            #.#.###.#.#.#.#.......#.#.#.#...#...#####...###.#.#.#.###...#...#.#.#.....#...........#.#.###.......#...........#.#.....###.....#...#.#.....#
            #.#.###.#.#.#.#.#######.#.#.###.#.#############.#.#.#.#######.###.#.#.###.#.###########.#.#########.#.###########.#####.###.###.#####.#.###.#
            #.#.#...#.#.#.#.#.....#.#...###.#.....#...#...#.#...#...#...#.....#...#...#.............#.###...#...#...........#.#...#.#...#...#...#...#...#
            #.#.#.###.#.#.#.#.###.#.#######.#####.#.#.#.#.#.#######.#.#.###########.#################.###.#.#.#############.#.#.#.#.#.###.###.#.#####.###
            #...#.....#...#...#...#.....#...#...#.#.#.#.#.#.#.......#.#.###.......#.#...#.....#.......#...#.#.#.........#...#.#.#.#.#...#.#...#...#...###
            ###################v#######.#.###.#.#.#.#.#.#.#.#.#######.#.###v#####.#.#.#.#.###.#.#######.###.#.#.#######.#.###.#.#.#.###.#.#.#####.#.#####
            #...#...#...#.....#.>.#.....#.#...#.#...#...#.#.#.#.....#.#.#.>.>...#...#.#.#.#...#.###...#.###...#.#.....#...###.#.#.#.#...#.#...#...#.....#
            #.#.#.#.#.#.#.###.#v#.#.#####.#.###.#########.#.#.#.###.#.#.#.#v###.#####.#.#.#.###v###.#.#.#######.#.###.#######.#.#.#.#.###.###.#.#######.#
            #.#.#.#.#.#.#...#.#.#...#...#.#.#...#.......#.#.#.#.#...#.#...#...#...#...#.#.#...>.>.#.#.#.......#...###.......#...#...#...#.....#.......#.#
            #.#.#.#.#.#.###.#.#.#####.#.#.#.#.###.#####.#.#.#.#.#.###.#######.###.#.###.#.#####v#.#.#.#######.#############.###########.#############.#.#
            #.#...#...#.....#.#.....#.#...#.#...#.#.....#...#.#.#.....#.......###...###.#.###...#...#.#.......###...........###.......#...#.........#.#.#
            #.###############.#####.#.#####.###.#.#.#########.#.#######.###############.#.###.#######.#.#########.#############.#####.###.#.#######.#.#.#
            #.......#.......#.......#.....#.###...#.........#...#.....#...............#.#.#...#...###...###.....#.....#...#...#.....#.....#.#.......#...#
            #######.#.#####.#############.#.###############.#####.###.###############.#.#.#.###.#.#########.###.#####.#.#.#.#.#####.#######.#.###########
            #...###...#...#.#...........#...#...#...........#.....#...#.......#.......#...#.....#.....#...#...#.#.....#.#.#.#...###.....#...#.#...#.....#
            #.#.#######.#.#.#.#########.#####.#.#.###########.#####.###.#####.#.#####################.#.#.###.#.#.#####.#.#.###.#######.#.###.#.#.#.###.#
            #.#.#...#...#.#...#.....#...#.....#...#.........#.....#...#.....#...#.....###...#...#...#...#.#...#...#.....#.#...#.....#...#.#...#.#.#.#...#
            #.#.#.#.#.###.#####.###.#.###.#########.#######.#####.###.#####.#####.###.###.#.#.#.#.#.#####.#.#######.#####.###.#####.#.###.#.###.#.#.#.###
            #.#.#.#.#...#.#.....###...###.........#.#.......#...#...#.#...#.#...#.#...#...#...#...#...#...#.........#...#.#...#.....#.....#.....#...#...#
            #.#.#.#.###.#.#.#####################.#.#.#######.#.###.#.#.#.#.#.#.#.#.###.#############.#.#############.#.#.#.###.#######################.#
            #.#...#.....#...#.......#...#...#...#...#.......#.#.....#.#.#.#.#.#.#.#...#.........#...#...#.....#...###.#...#...#.#...###.................#
            #.###############.#####.#.#.#.#.#.#.###########.#.#######.#.#.#v#.#.#.###.#########.#.#.#####.###.#.#.###v#######.#.#.#.###v#################
            #.#...#...........#.....#.#.#.#.#.#.###.........#.......#.#.#.>.>.#.#.#...###...#...#.#.#...#...#.#.#.#.>.>.#...#.#.#.#.#.>.#...#...#...#...#
            #.#.#.#.###########.#####.#.#.#.#.#.###.###############.#.#.#######.#.#.#####.#.#v###.#.#.#.###.#.#.#.#.###.#.#.#.#.#.#.#.#v#.#.#.#.#.#.#.#.#
            #.#.#.#.#...........###...#.#.#.#.#...#...........#.....#.#.....#...#.#...#...#.>.>.#.#.#.#...#.#.#.#.#.#...#.#.#.#.#.#.#.#.#.#...#...#.#.#.#
            #.#.#.#.#.#############.###.#.#.#.###.###########.#.#####.#####.#.###.###.#.#######.#.#.#.###.#.#.#.#.#.#.###.#.#.#.#.#.#.#.#.#########.#.#.#
            #.#.#.#.#.#...#.........#...#.#...#...#...........#.....#.#...#.#.....#...#...###...#.#.#.###.#.#...#.#.#...#.#.#.#.#.#.#.#.#.#.........#.#.#
            #.#.#.#.#.#.#.#.#########.###.#####.###.###############.#.#.#.#.#######.#####.###.###.#.#.###.#.#####.#.###.#.#.#.#.#.#.#.#.#.#.#########.#.#
            #.#.#.#.#.#.#...#.......#...#.....#...#.....#...#####...#.#.#.#.#.......#...#...#...#.#.#...#.#...#...#.#...#.#.#.#...#.#.#.#.#.....#...#.#.#
            #.#.#.#.#.#.#####.#####.###.#####.###.#####.#.#.#####.###.#.#.#.#.#######.#.###.###.#.#.###.#.###.#.###.#.###.#.#.#####.#.#.#.#####.#.#.#.#.#
            #...#...#...#.....#...#.....#...#.#...###...#.#...#...###.#.#.#.#.#.....#.#.#...###.#.#...#.#...#.#.#...#.#...#...#...#.#.#.#.#.....#.#...#.#
            #############.#####.#.#######.#.#.#.#####v###.###.#.#####.#.#.#.#.#.###.#.#.#.#####.#.###.#.###.#.#.#.###.#.#######.#.#.#.#.#.#.#####.#####.#
            #.............#...#.#.#.....#.#.#.#...#.>.>.#...#.#.....#.#.#.#.#.#.#...#.#.#.....#.#.#...#...#.#.#.#...#.#...#.....#...#.#.#.#...#...#.....#
            #.#############.#.#.#.#.###.#.#.#.###.#.###.###.#.#####.#.#.#.#.#.#.#.###.#.#####.#.#.#.#####.#.#.#.###.#.###.#.#########.#.#.###.#.###.#####
            #...............#...#...###...#...###...###.....#.......#...#...#...#.....#.......#...#.......#...#.....#.....#...........#...###...###.....#
            ###########################################################################################################################################.#
            """;

    List<LinkedList<Position>> deplacements = new ArrayList<>();
    private String[][] grille;

    @Test
    void test() {
        grille = Arrays.stream(entree.split("\n"))
                .map(s -> s.split(""))
                .toArray(String[][]::new);


        var positionIntiale = calculerPositionDebutFin(0);

        // Condition d'arrêt avoir atteint la position de fin
        var positionFin = calculerPositionDebutFin(grille.length - 1);

        // Init du 1er déplacement
        LinkedList<Position> deplacementInit = new LinkedList<>();
        deplacementInit.add(positionIntiale);
        deplacements.add(deplacementInit);

        // Tant que tous les chemins n'ont pas atteint la cible
        while (!deplacements.stream()
                .map(LinkedList::getLast)
                .allMatch(positionFin::equals)) {

            deplacer(positionFin);
        }
        var longueurs = deplacements.stream()
                //.peek(this::afficherChemin)
                .map(List::size)
                .sorted()
                .toList();

        log.debug("{} déplacement identifiés avec pour longeurs = {}", deplacements.size(), longueurs);

        var sortie = longueurs.stream()
                .mapToInt(i -> i)
                .max()
                .orElseThrow();

        // Retire la position initiale
        log.debug("Sortie : {}", sortie - 1);
    }

    void deplacer(Position positionFin) {
        deplacements = deplacements.stream()
                .parallel()
                .map(c -> deplacerChemin(c, positionFin))
                .flatMap(Collection::stream)
                // DEBUG
                //.peek(this::afficherChemin)
                .toList();
    }

    List<LinkedList<Position>> deplacerChemin(LinkedList<Position> chemin, Position positionFin) {

        var retour = new ArrayList<LinkedList<Position>>();
        var position = chemin.getLast();

        if (!positionFin.equals(position)) {

            if (peutMonter(position) && !chemin.contains(monter(position))) {
                var nouveauChemin = (LinkedList<Position>) chemin.clone();
                nouveauChemin.add(monter(position));

                retour.add(nouveauChemin);
            }

            if (peutDescendre(position) && !chemin.contains(descendre(position))) {
                var nouveauChemin = (LinkedList<Position>) chemin.clone();
                nouveauChemin.add(descendre(position));

                retour.add(nouveauChemin);
            }

            if (peutDroite(position) && !chemin.contains(droite(position))) {
                var nouveauChemin = (LinkedList<Position>) chemin.clone();
                nouveauChemin.add(droite(position));

                retour.add(nouveauChemin);
            }

            if (peutGauche(position) && !chemin.contains(gauche(position))) {
                var nouveauChemin = (LinkedList<Position>) chemin.clone();
                nouveauChemin.add(gauche(position));

                retour.add(nouveauChemin);
            }
        } else {
            // On veut garder le chemin si terminé
            retour.add(chemin);
        }

        return retour;
    }

    boolean peutMonter(Position position) {
        var x = position.x();
        var y = position.y();

        return y > 0 && ".".equals(grille[y - 1][x]);
    }

    boolean peutDescendre(Position position) {
        var x = position.x();
        var y = position.y();

        return y < grille.length - 1 && List.of(".", "v").contains(grille[y + 1][x]);
    }

    boolean peutDroite(Position position) {
        var x = position.x();
        var y = position.y();

        return x < grille[y].length - 1 && List.of(".", ">").contains(grille[y][x + 1]);
    }

    boolean peutGauche(Position position) {
        var x = position.x();
        var y = position.y();

        return x > 0 && ".".equals(grille[y][x - 1]);
    }

    Position calculerPositionDebutFin(int ligne) {
        for (int x = 0; x < grille[ligne].length; x++) {
            if (".".equals(grille[ligne][x])) {
                return Position.of(x, ligne);
            }
        }
        throw new RuntimeException("Impossible de déterminer la position départ/fin pour la ligne " + ligne);
    }

    Position monter(Position position) {
        return deplacerPosition(position, x -> x, y -> y - 1);
    }

    Position descendre(Position position) {
        return deplacerPosition(position, x -> x, y -> y + 1);
    }

    Position gauche(Position position) {
        return deplacerPosition(position, x -> x - 1, y -> y);
    }

    Position droite(Position position) {
        return deplacerPosition(position, x -> x + 1, y -> y);
    }

    Position deplacerPosition(Position position,
                              Function<Integer, Integer> deplacementX,
                              Function<Integer, Integer> deplacementY) {

        return Position.of(
                deplacementX.apply(position.x()),
                deplacementY.apply(position.y())
        );
    }

    void afficherChemin(LinkedList<Position> chemin) {
        var grilleAffichage = new String[grille.length][grille[0].length];
        copieGrille(grilleAffichage);
        chemin.forEach(p -> grilleAffichage[p.y()][p.x()] = "O");

        var affichage = Arrays.stream(grilleAffichage)
                .map(a -> String.join("", a))
                .collect(Collectors.joining("\n"));

        log.debug("\n{}", affichage);
    }

    void copieGrille(String[][] destination) {
        for (int i = 0; i < grille.length; i++) {
            System.arraycopy(grille[i], 0, destination[i], 0, grille[i].length);
        }
    }
}
