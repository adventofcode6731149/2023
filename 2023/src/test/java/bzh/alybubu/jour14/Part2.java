package bzh.alybubu.jour14;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

/**
 * TODO : WIP
 * Puzzle non fini !
 */
@Slf4j
public class Part2 {

    private final String entree2 = """
            O....#....
            O.OO#....#
            .....##...
            OO.#O....O
            .O.....O#.
            O.#..O.#.#
            ..O..#O..O
            .......O..
            #....###..
            #OO..#....
            """;

    private final String entree = """
            O..##O.#..#..#...##O#.#...OO....O.#.....O......#OO...#...OO....O....O....O#.O.....O.#.O..O.O#......#
            .O.#..O...#....#...O..OO..#..O.O.....O.O...#O....#O.#O...OO#.O.#O##.......#.#OO#.O.O...O....O.....O#
            O.O.O......#..#.O.#O........##.#...#...#....O........#O#...#.O...O.O..O.OO#.#..#.#....O..O.#.#..O.O.
            ......##O.......#O.OO.OO#..#O....#OO...O......O.OO#O#...#....#.O.#.O#..O..O.O...OOO..O........#...O#
            #...###.O.#O#OOO#O...OO..O.O.##.#.........O.........OO.OOO#...O.#O....#.......O.OO#.#...O..#....O..O
            .O....#...#.#.OO.O#..O.....O...OO..##..O.....#..O.OO..#.O..O..##...O..OO...O#...O.....##.O..O.O.O.##
            ...#.#O..OO.#..#.#.##...O.OO.O#.O##OO.O..#...#O.....OO..O.###.#...#.#O....##...#O...#..O.#...##.....
            ...O...O..O...#O#.##....#..O#..O..............O.....O..#........##...O.##..#.O.O.O......OO#O.#..#OOO
            .#O......O..#..O..#.......#...#O....O..O#.........O.O....O....OO....O#....OOO.OO..O.#.O#........#.OO
            #.O.O.O.........O.#...O...#.O#.O.O#O#...................##.OO..#.O.......#...O##O.O.......O...O.OOOO
            ....#.#.O...OO.O..#.O..O...#.OO.#O...O.O.O..........O..#...OO..##.O.OOO...O#....O..O....O..OO.O..O..
            OO.#...#...O#..O##..OO#O.O.O.#OO..O....O#O..#O#.##OO........OO..O##.#.#.....##...O........O..O.O.O..
            .O...O..O##...O....O.....O..#...#.#..##O#.O...O.O....#........O......#O#O#.#O#......O..O#..O....O...
            O.O#....O#.OO##OO.#.O..#.O#.....#...#O#.#O.....OO#.......OOO.#..O..#OO.#...#.O.#O.O.O#.O.O..O..#.#..
            ##....#...###.......O...O........O.....O.OOOO.#.##...........O.O#O..#.#.#...#....#O.O#.#OO..##.....#
            .#..#...#.....O.O.##..O#O#..##..#.#O.....O..#O##..#......O..#..#.#.O..#O..#.......O#...O#.OO..O.O...
            #.....O....#......##.O......O..##....#.O...........O.O#.O##O...#.##..O.#.........#......#.O....OO...
            .#.OO..O...#...O..###.O#.O.O..........#...#O..###O##..#.O.OO..#O.O..OO#.##.##...O...OOO....O.....O.#
            .O..#...O.#...O.#............#...#.##....#.O.O#.....O....O..#..OO#O..#.#.OO........O..O..#O..#...O#.
            .O..O..#O..O.OOOO...O....#...#.#.#.OO..OO.#....O#.......#..OO####O..O#O...O..O##...O#...#.........#.
            ..##O...##...##...O..##...#.....O##.....#..O...O..........O.OOOO...#O#.O###.O.#O......OO...O..#OO#..
            O..#.O........#..OO...#O..OO...#.....OOOO........OO.......#O.O..#OO...O..#......O.#.OO..O....O....#.
            .O#.O.#.O..#.OO...........#..#O.#...O.......#...........#.##..O#O...O..#...O.O...#........O..O.OO.#.
            #OO.........#...O....O.O......#.O#.....OO.#O.OO..O..O............OO.....O....O.O...O.O....O.....OO..
            ...#O...#...#O#..OO.O..#........#O..O.O.O......O....#...O.O..O....#O...O...#.##.O...#...#.OO.....O..
            ..#.O#.O#..###..O#.O.O..O.#......O..OO.O..O.....O..##.#........O#.O.##O.#....#.O#.#.O#O..OO...#..#OO
            OOOO.#..#....OO...O.##O#.#O...O..#O.O.OO....#O#.#O.O...#....O......#....O.......#.O.O.OOO...#OO..O.O
            #...O#OO##O.OO.....O.O......O..O....O.O..O......#OO..O....#....OOOO#.....O#OOO#....O.O....OO...#O..#
            .#O..#.###...O...#.O.#.....O.O.....#OOOO.#..O.#O.#....##..#.#..O#.#.....O....O.O#O...O...OO.###.....
            ........OO###.#..............O..O.OOO.#O.O...O.OO...#O.O.....#O#OOO.#O#..O#.#...O..#..##.#..O.......
            ........O#.#.O...OO.O...##..#..O....#......OO...#...O.......#O.#....#O#...O...O...OO...OO.#.O#...OOO
            O....O.............#.OO#O..O#..O#.##.#..O..O.##....##O...##.OO......#OOO...#OO..OO#......O.O..O.O...
            .......OO.#..O.O##...#..OOO.#.O..........................###.OO#O.O#.#..........O....O...##....OO..O
            .#..O..O.OOO#OO.#O...#...O...O#OO.O......#..O.##....O.....#...O...O#..#...###O......####.....O##....
            .O#O..##...O..#OO.....#...........#.#O.#..O..OO.O....OO.#.O..#....##.O#....O.#..#.#.....#.#O.O.O...#
            ...O##..O#.#.O..#........O.##......O.O.O#O.#O..O...O..OO...O...O.O.O#O...O.O..OO..OOO..#.O#OO....O..
            .#...O.....O#..O.#...O.O....##O....#...#O...O..#......O#....#OO.O.#..#.....##.O.#OO..O#.......O..O#O
            ......#O.#.....###.O...O.............OO.......O..#......O....#.O.#.....O#O..#....#O......O.O..#.....
            #O###..O.O#.O#.#.O..#.......#.##.O.O......O......###...#.OO.OO.##.O#O.#.......##.O...O#O.O..##..#.#.
            ...OO...##O#O.#..O..O...O.O..#....###.....#......#.O....#..O.#.....O##.O....OOO.#..#O###.O.O.O..#.O.
            #.#.....#.OO...O...O#...O...#..#..#.O.OO.#OO#...OO#.....#.....O....O.O....O...O.#...O...OOOO...O..O.
            OO..OOO.#O.O.O...#.###O..OOO..#....O.....#...##....#..#....O.O#...O....#.....O.O.#O...O.....OOO....#
            ...##.O.#O#O....O......O#.#..........#.#.O...O...##..O.##.......O...O.###....O...#..O..O..O.....O..O
            .O......#OOO.....#OO...OO.O.##...#......#O.....O.....O..##O......#OO.O......O#......#O##.#OO..O..O..
            ...#O..#...O#..O........O..O.....OOO.#.O.O..##.....O##..###..#..O#O....O......#..O.#.O.O.#.....OO#O.
            O...#.O##.O..#.#O.......#....#..##.#O#.OO......O.#...OOO##.#.O..O.O..#..#O......#..O.....OO.......##
            ....OO....#O.O#O#.....O.O....O...#.#OO...O......#O.O#..#.O....#.....O...O..........#.O....##O.O#.#O#
            ......#.O....OOO#.O...#..O...OO#..#..............O#..O.#.........O...#.O.......O............O..#O#..
            #.#........O.O....O#..OO..#...O.#O#..O.OO....#O...###.OO.......#..O...##.#..##.....O##.O..........#.
            #O..#...O.....O.O.O...#O.#..O....O..#.#..O.O.#O...O##O##.OO...O....OO##.....##.#.O.#..OOO..O.#O..#O.
            ........#.O..O..#......O.#.O..#OOOOO..#O...#.....O.#..O.....O.#.O.OO.O.#.....O..........OO.......O.#
            ...O..##.O...#....O.O....#..#.#...OO...#...O.#.O.....#...O..O.....#....##.#O..#.#....##...OO....O.#.
            .....#..##O...#O..O#O.#..##.O.O.O..#.O##.O#........O....#.O##O..#O##O#...O..##O#.#.OO...O##.#....O..
            O.OO..O#...OO......O...O.O........##....OO.O..#...O........#.....##.O....#.O.O.O#.......O.O#O....O..
            #.O....#.....O.OO#...#..O.....#..OO...OO#....O....#........O......#...#..#O#O....#.#........O..##.O#
            #..OO..O......#O.O#.O...#.#....O.O#.O..#.OOO#.O##.....#.OO...O.O..#..##..O.#O#....O.#....OO..#......
            #..OO..#..#.#O.OOO##.O..O#.#O..O..O.....O...O...#...#..........#...O..#O...#..OO...#..O.#OOO...#....
            ......#.O....#..OO.OO...#...O...........#O.OO...#.#..O.O...##....#..#........#......#.O..#..O##O....
            #.##..#O##....O..........O.O.##..#O..O..#.OO.O.O.#....#.....OOOO..##.#OO.#.O.#O......#.#.#.O..OO..#.
            #O..#..O..O....O#.OO...#.O.#..O.##.O.....OO.O....O.....O.#.O....O....O....##O...O..O.OO.......O#..O.
            .O.....#.#.........OO..........#.O.#.O#.....#.....O...O.#O.............O#.#..O..O........#.##O..#...
            O.....#....O...#OOO##..####.#.....O.....#...#...#...##..........#..####............O...OOO.O.#...#..
            .#O#OOO..#..O..#.O#.O#.O.O##.....O#..##O....#..O#......O#...O#.O.....O.O#..OO........O..O..#.#.....O
            .O#...#...#O#.....##.#O.#...#......O..O.O.........OOOO..#.#....OO.#.O.O..#.O...O.#.#...OO..#.OOO..#O
            #..#..#..#.#.#.......O#...O.#O..O..#.......#..O.#.....#.#.O.O##..#..##O.#......O...O...#O.OOO#.O.##.
            .O......OO..OO.O..##.#.#..O....O...O..##...........O..................O.#...O...O..O.#.O..OOOO..#..O
            .OOOOO...O......##.#..O..O.##.OO#.#.##.##O..#........#....O..#..O#..O.OOO....O#..O......O.O..#...#.#
            OO..#.#O...#.O.O.O.#.O.#...O...O.##..##..#.#..O....O.......#...#.#OO##.O.OOO...O#.....O.##......OO..
            ...O..O.....###O....OO#....#OO....O##.###.#O..#..OO..O.O#.O......##.O#.O#O....O..#.OO#O.#..O...OO...
            ..##..#..OOOOO#O..#O.##..OO.#.O.......O...O##.#.O...O...O..O..O..#.O......#....#O..#.#.#.......O....
            ....O.............O....O..##.OOO.#.#....#O..O..........#..#O..##.O##O.O...........O....#.....OO..OO.
            ....O...#..O.....OO..#...O.OO#..O.##.O..O.##.#.O..#.#O......#.OO....#....##..#...O#......#O......O..
            .....#O#...#..O..OOO#O....O..O..O.OO.........##.O..#O#......O..O.O.O.OO#.#.O.O..O..O..O....#...#O...
            ..O#O##...#.O...........#O..##.O.O.#..O...O#O.#.O.........O.O...#.#O....OO#O#.O#..O..#.#O.O.OO#O....
            ........OO..#...O.....O................#..O..........#.#.O.....#..OO.....O...O..O....#..#..........#
            #..OO.O......O.#..OO.O##...OO.....O#.O.....O#....##.##.O.OOO.O...O..#.O....#OO...OO..#.#.......O....
            ..O...#O.....O.OO.#..#.#O.O##O.O...#.O#...O.#O...#O....#..O..O..O.....#OO.O..O..##.O....##.....#OO..
            ..O..O.#....#.#OO.......O.OO..O.........O....#...O#.#..O.O...O.O..###OO..O#..#.....O.O.....#.#O..O.O
            .#O..O#.....#...OOO..#OO#.O.......O..O......OO...O.#.O##O...##...#.....O...O....O.O.##...#..........
            O.##..#...#.O...#.##..O...#..O.#O.O##.##.O..O.O..O.OO..O....#.#O..O.O.O....#.#..O#..#O..#....O......
            OO##OO...O.#.OOO...#O..#O......O..O#.O.......#....OO.O.#...O.O.......O#O........##.#.O....#....O..#.
            .O.....O#..#...#.....#.O..O.#.#O......##....OO....O...#.#.#........O.........OO....O#...##OO...O.O.#
            O#.##...O.#OO...##.OO#.....O.O.OO#...#....O.O..O.O.#.OO.O...O#...O#.#OO..O#.#.#O....O....O#.#O.OOO#.
            ##..#.O#.OOO#.......O.#O.O#......O.####OO......O..O..OO.....#.O..O#.OO..O.#O.#.O.O..##OO....#.......
            .#......O..#..#...OO......O...O....O...O..........O.O..O..O..###...OOO..O..O..O.O........O.O........
            #OO.O..O.OO......#.##.......O..O#.OOO..#..O.#........##.#.O....O.#O#..O...#............O...O....#...
            .#...#O..#.....O...O#.O...#O..........##.#OO..##..O#..O.#.O...O#..O..O.OO.O.####O....##.O......###O.
            .O.#OO#O..................#..#...O.#.O.#..OO.O.O.............###..O..OO##.O..........O#..O......##.#
            .O.O..O.....OOO..#O.O.###.O.....OO#O....#...#....O......#.........#...##.O##O...##....O#OO#.O...#.O.
            ##..#OO#O..#..OO#.O#OOOO.O.##O...OO..O.O.O.OO##O.O.O.O.O...OO...O.##....O...##O#.OO#.#...O......#O.#
            .#..#.#.O......OOOO....O#....#...O#O..OOO.#O#OO......##....O##..#.O.O...........O.O....##...O.......
            #....O.........O#O.O.....O.###.###.O......#..OO..#.O...O##..O.O..OO..#.O....#...#.##O.O.OO...#O.OO..
            ...O..OOO......O.#O.#.##.......#.#...#...OOO....#........O....OO.OO.O..O##..O#O.......##.O....O.....
            ...#.O#.#.O#.OO...#..##........#..O##..#O.#.......##O....O..O...O...#..#O...O.......#..O#.#..#O.#...
            O....O...OO.O...OO.O..O...O..##O..O...O...O#.#....O.........#.O#.O.#..#.O.O.O.O....O#.O..##OO..OO...
            .#..#.OO...OO..OO...OO..#.OO.....#.##..##.O...O.#..#..#....#.O.O.......OO##..##..O.O...O..OOO.......
            O...O..#.....O#O..##..O..#OO....#.#O#.O.OO.O.O.OO#....##.#.O....O..#O#.#....#O..#O.##.O..OO...#.....
            ..#....O#.#.##...##..O....##.#O.......O#O..#....#.#.........OO..####O...O##..O#OO...O......#O..#..#.
            ...#.....O.O.O.#.#..#..#O.O..O...O.#.O..#...#....O......#.OO...O....O.O.#.#...O....##....##.OO......
            ..##O....O.#.#..#......OO#O.O..#.O.O..#O.OO...#....O..O#O........#OOOO....#O...#....#O##..#O#....#..
            """;

    private String[][] tableau;

    @Test
    void jour14() {
        tableau = Arrays.stream(entree2.split("\n"))
                .map(s -> s.split(""))
                .toArray(String[][]::new);

        afficherTableau();

        for (int i = 0; i < 1000000000; i++) {
            cycle();
            if (i % 100000 == 0) {
                log.debug("Cycle {}", i);
            }
        }

        // Debug
        //afficherTableau();

        var sortie = calculerScoreTableau();

        log.debug("Sortie : {}", sortie);
    }

    void cycle() {
        // Nord
        haut();

        // Ouest
        gauche();

        // Sud
        bas();

        // Est
        droite();
    }

    void haut() {
        while (peutMonter()) {
            monter();
        }
    }

    void bas() {
        while (peutDescendre()) {
            descendre();
        }
    }

    void droite() {
        while (peutDroite()) {
            allerDroite();
        }
    }


    void gauche() {
        while (peutGauche()) {
            allerGauche();
        }
    }

    long calculerScoreTableau() {
        var somme = 0;
        for (int x = 0; x < tableau.length; x++) {
            for (int y = 0; y < tableau[x].length; y++) {
                if ("O".equals(tableau[x][y])) {
                    somme += tableau.length - x;
                }
            }
        }

        return somme;
    }

    boolean peutDescendre() {
        return verifierDeplacementGlobal(this::peutDescendreUnitaire);
    }

    boolean peutMonter() {
        return verifierDeplacementGlobal(this::peutMonterUnitaire);
    }

    boolean peutDroite() {
        return verifierDeplacementGlobal(this::peutDroiteUnitaire);
    }

    boolean peutGauche() {
        return verifierDeplacementGlobal(this::peutGaucheUnitaire);
    }

    boolean verifierDeplacementGlobal(BiFunction<Integer, Integer, Boolean> functionControle) {
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < tableau[i].length; j++) {
                if (functionControle.apply(i, j)) {
                    return true;
                }
            }
        }
        return false;
    }

    void monter() {
        // Pas de parallèlisation possible ici (ligne par ligne séquentiellement)
        for (int i = 0; i < tableau.length; i++) {
            final var x = i;
            IntStream.range(0, tableau[x].length)
                    .parallel()
                    .forEach(y -> monterUnitaire(x, y));
        }
    }

    void descendre() {
        // Pas de parallèlisation possible ici (ligne par ligne séquentiellement)
        for (int i = tableau.length - 1; i >= 0; i--) {
            final var x = i;
            IntStream.range(0, tableau[x].length)
                    .parallel()
                    .forEach(y -> descendreUnitaire(x, y));
        }
    }

    void allerGauche() {
        IntStream.range(0, tableau.length)
                .parallel()
                .forEach(x -> {
                    // Pas de parallelisation possible ici car colonne par colonne
                    for (int y = 0; y < tableau[x].length; y++) {
                        gaucheUnitaire(x, y);
                    }
                });
    }

    void allerDroite() {
        IntStream.range(0, tableau.length)
                .parallel()
                .forEach(x -> {
                    // Pas de parallelisation possible ici car colonne par colonne
                    for (int y = tableau[x].length - 1; y >= 0; y--) {
                        droiteUnitaire(x, y);
                    }
                });
    }

    void monterUnitaire(int x, int y) {
        if (peutMonterUnitaire(x, y)) {
            tableau[x - 1][y] = "O";
            tableau[x][y] = ".";
        }
    }

    void descendreUnitaire(int x, int y) {
        if (peutDescendreUnitaire(x, y)) {
            tableau[x + 1][y] = "O";
            tableau[x][y] = ".";
        }
    }

    void gaucheUnitaire(int x, int y) {
        if (peutGaucheUnitaire(x, y)) {
            tableau[x][y - 1] = "O";
            tableau[x][y] = ".";
        }
    }

    void droiteUnitaire(int x, int y) {
        if (peutDroiteUnitaire(x, y)) {
            tableau[x][y + 1] = "O";
            tableau[x][y] = ".";
        }
    }

    boolean peutMonterUnitaire(int x, int y) {
        var valeur = tableau[x][y];

        // Seuls les pierres rondes bougent
        if (valeur.equals("O")) {
            if (x == 0) {
                return false;
            } else {
                // peut bouger seulement si la case au dessus est du terrain
                return tableau[x - 1][y].equals(".");
            }
        } else {
            return false;
        }
    }

    boolean peutDescendreUnitaire(int x, int y) {
        var valeur = tableau[x][y];

        // Seuls les pierres rondes bougent
        if (valeur.equals("O")) {
            if (x == tableau.length - 1) {
                return false;
            } else {
                // peut bouger seulement si la case au dessus est du terrain
                return tableau[x + 1][y].equals(".");
            }
        } else {
            return false;
        }
    }

    boolean peutDroiteUnitaire(int x, int y) {
        var valeur = tableau[x][y];

        // Seuls les pierres rondes bougent
        if (valeur.equals("O")) {
            if (y == tableau.length - 1) {
                return false;
            } else {
                // peut bouger seulement si la case au dessus est du terrain
                return tableau[x][y + 1].equals(".");
            }
        } else {
            return false;
        }
    }

    boolean peutGaucheUnitaire(int x, int y) {
        var valeur = tableau[x][y];

        // Seuls les pierres rondes bougent
        if (valeur.equals("O")) {
            if (y == 0) {
                return false;
            } else {
                // peut bouger seulement si la case au dessus est du terrain
                return tableau[x][y - 1].equals(".");
            }
        } else {
            return false;
        }
    }

    void afficherTableau() {
        var builder = new StringBuilder();
        for (String[] strings : tableau) {
            builder.append(String.join("", strings));
            builder.append("\n");
        }
        log.debug("\n\n{}", builder);
    }
}
