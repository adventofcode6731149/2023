package bzh.alybubu.jour4;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

@Slf4j
public class Part2 {

    private int total = 0;

    private String entree() {
        return """
                Card   1:  4 16 87 61 11 37 43 25 49 17 | 54 36 14 55 83 58 43 15 87 17 97 11 62 75 37  4 49 80 42 61 20 79 25 24 16
                ....
                Card 193: 53 40  5 39 13 12 27 57 68 45 | 67 10 87 64 22  6 77 17 20 24 78 52 19 18 99 88 66 31 65 47 11 61 90  9 92
                """;
    }

    @Test
    void test() {
        var entree = entree().replaceAll("Card[ ]+[0-9]+:[ ]+", "")
                .replaceAll("[ ]+", " ");

        var original = entree.split("\n");
        var travail = new Stack<Pair<Integer, Pair<List<String>, List<String>>>>();

        for (int i = 0; i < original.length; i++) {
            total++;
            travail.push(transformerLigne(Pair.of(i, original[i])));
        }

        while (!travail.isEmpty()) {
            ajouterCopieLigne(travail.pop(), travail, original);
        }

        log.debug("Sortie : {}", total);
    }

    private Pair<Integer, Pair<List<String>, List<String>>> transformerLigne(Pair<Integer, String> ligne) {

        var second = Optional.of(ligne.getRight())
                .map(s -> s.split(" \\| "))
                .map(Arrays::asList)
                .map(l -> Pair.of(l.get(0), l.get(1)))
                .map(p -> Pair.of(
                        Arrays.asList(p.getLeft().split(" ")),
                        Arrays.asList(p.getRight().split(" ")))
                )
                // N'arriver pas -> Juste pour copier/coller le code existant de la V1
                .orElseThrow();

        return Pair.of(ligne.getLeft(), second);
    }

    void ajouterCopieLigne(Pair<Integer, Pair<List<String>, List<String>>> ligne,
                           Stack<Pair<Integer, Pair<List<String>, List<String>>>> stack,
                           String[] original) {
        int index = ligne.getLeft();
        var second = ligne.getRight();

        var gagants = second.getLeft();
        var chiffres = second.getRight();

        var commun = CollectionUtils.intersection(gagants, chiffres);

        if (commun.isEmpty()) {
            // RAF
        } else {
            // Ajout n cartes
            var ajout = commun.size();
            var indexMax = Math.min(index + ajout, original.length - 1);
            for (int i = index + 1; i <= indexMax; i++) {
                total++;
                stack.push(transformerLigne(Pair.of(i, original[i])));
            }
        }
    }
}
