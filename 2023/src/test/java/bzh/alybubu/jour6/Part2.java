package bzh.alybubu.jour6;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.stream.LongStream;

@Slf4j
public class Part2 {

    @Test
    void jour5() {
        var sortie = nombreCombinaisonGagnante(Pair.of(60808676L, 601116315591300L));

        log.debug("sortie : {}", sortie);
    }

    long nombreCombinaisonGagnante(Pair<Long, Long> entree) {
        var temps = entree.getLeft();
        var distance = entree.getRight();

        return LongStream.range(0, temps)
                .parallel()
                .map(i -> calculerGagnant(i, distance, temps))
                .filter(l -> l == 1)
                .count();
    }

    int calculerGagnant(long temps, long distanceMax, long tempsTotal) {
        var distance = temps * (tempsTotal - temps);
        if (distance > distanceMax) {
            return 1;
        } else {
            return 0;
        }
    }
}
