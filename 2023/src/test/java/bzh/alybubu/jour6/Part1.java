package bzh.alybubu.jour6;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

@Slf4j
public class Part1 {

    private final List<Pair<Integer, Integer>> entree = List.of(
            Pair.of(60, 601),
            Pair.of(80, 1163),
            Pair.of(86, 1559),
            Pair.of(76, 1300)
    );

    @Test
    void jour5() {
        var sortie = entree.stream()
                .mapToInt(this::nombreCombinaisonGagnante)
                .reduce(1, Math::multiplyExact);

        log.debug("sortie : {}", sortie);
    }

    int nombreCombinaisonGagnante(Pair<Integer, Integer> entree) {
        var temps = entree.getLeft();
        var distance = entree.getRight();

        return IntStream.range(0, temps)
                .map(i -> calculerGagnant(i, distance, temps))
                .sum();
    }

    int calculerGagnant(int temps, int distanceMax, int tempsTotal) {
        var distance = temps * (tempsTotal - temps);
        if (distance > distanceMax) {
            return 1;
        } else {
            return 0;
        }
    }
}
