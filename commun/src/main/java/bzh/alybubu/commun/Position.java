package bzh.alybubu.commun;

import java.util.function.IntUnaryOperator;

public record Position(int x, int y) {
    public static Position of(int x, int y) {
        return new Position(x, y);
    }

    private static Position transformer(Position positionInitiale,
                                        IntUnaryOperator transfoX,
                                        IntUnaryOperator transfoY) {
        return Position.of(
                transfoX.applyAsInt(positionInitiale.x()),
                transfoY.applyAsInt(positionInitiale.y())
        );
    }

    public static Position deplacerHaut(Position position) {
        return transformer(position, x -> x, y -> y - 1);
    }

    public static Position deplacerBas(Position position) {
        return transformer(position, x -> x, y -> y + 1);
    }

    public static Position deplacerDroite(Position position) {
        return transformer(position, x -> x + 1, y -> y);
    }

    public static Position deplacerGauche(Position position) {
        return transformer(position, x -> x - 1, y -> y);
    }
}
